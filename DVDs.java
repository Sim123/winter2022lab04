public class DVDs {
	private String title;
	private String rating;
	private double price;
	private int lengthOfMovie;
	
	//getter
	public String getTitle() {
		return this.title;
		}
	public String getRating() {
		return this.rating;
		}
	public double getPrice() {
		return this.price;
		}
	public int getLengthOfMovie() {
		return this.lengthOfMovie;
		}
		
		//setter
	public void setRating(String newRating) {
		this.rating = newRating;
		}
	public void setPrice(double newPrice) {
		this.price = newPrice;
		}
	public void setLengthOfMovie(int newLengthOfMovie) {
		this.lengthOfMovie = newLengthOfMovie;
		}
		
		//constructor
	public DVDs(String newTitle, String newRating, double newPrice, int newLengthOfMovie){
		this.title = "Harry Potter and the Chamber of Secrets";
		this.rating = "PG";
		this.price = 28.99;
		this.lengthOfMovie = 161;
		}
	
	public void movieInfo() {
			if (this.rating.equals("G")) {
				System.out.println("It is rated " + this.rating + " which means that it is suted for General Audiences");
			} else if (this.rating.equals("PG")) {
				System.out.println("It is rated  " + this.rating + " which means that it requires some Parental Guidance");
			} else if (this.rating.equals("PG-13")) {
				System.out.println("It is rated  " + this.rating + " which means that it may be inappropriate for Children Under 13");
			} else if (this.rating.equals("R")) {
				System.out.println("It is rated  " + this.rating + " which means that it Restricted for Children Under 17");
			}else {
				System.out.println("Not sure what you're referring to!");
		}
	}
}